const jwt = require("jsonwebtoken");

// Middleware function to authenticate user using JWT token
function authenticateUser(req, res, next) {
  // Get the JWT token from the Authorization header
  const token = req.headers.authorization;

  // Check if the token is provided
  if (!token) {
    return res.status(401).json({ error: "No token provided" });
  }

  try {
    // Verify and decode the token
    const decoded = jwt.verify(token, process.env.JWT_SECRET);

    // Attach the user ID to the request object
    req.user = { id: decoded.userId };

    // Proceed to the next middleware or route handler
    next();
  } catch (error) {
    return res.status(401).json({ error: "Invalid token" });
  }
}

module.exports = {
  authenticateUser,
};
