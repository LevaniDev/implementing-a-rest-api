const mongoose = require("mongoose");

const dbURI = "mongodb://localhost:27017/todoapp";

module.exports = {
  jwtSecret: "your-jwt-secret",
  database: {
    connect: async () => {
      try {
        await mongoose.connect(dbURI, {
          useNewUrlParser: true,
          useUnifiedTopology: true,
          useCreateIndex: true,
          useFindAndModify: false,
        });
        console.log("MongoDB connected");
      } catch (error) {
        console.error("MongoDB connection error:", error);
      }
    },
  },
};
