const bcrypt = require("bcrypt");
const User = require("../Models/User");

// Function to register a new user
async function registerUser(email, password, firstName, lastName) {
  // Check if the email is already taken
  const existingUser = await User.findOne({ email });
  if (existingUser) {
    throw new Error("Email already exists");
  }

  // Hash the password
  const hashedPassword = await bcrypt.hash(password, 10);

  // Create a new user object
  const user = new User({
    email,
    password: hashedPassword,
    firstName,
    lastName,
  });

  // Save the user in the database
  await user.save();

  return user;
}

// Function to log in a user
async function loginUser(email, password) {
  // Find the user with the provided email
  const user = await User.findOne({ email });
  if (!user) {
    throw new Error("User not found");
  }

  // Compare the provided password with the stored hashed password
  const passwordMatch = await bcrypt.compare(password, user.password);
  if (!passwordMatch) {
    throw new Error("Invalid password");
  }

  // Return the user
  return user;
}

module.exports = {
  registerUser,
  loginUser,
};
