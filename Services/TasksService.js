const Task = require("../Models/Task");

// Function to add a task to the list
async function addTask(userId, title, description) {
  // Check if a task with the same title already exists for the user
  const existingTask = await Task.findOne({ userId, title });
  if (existingTask) {
    throw new Error("Task with the same title already exists");
  }

  // Create a new task object
  const task = new Task({
    userId,
    title,
    description,
    done: false,
  });

  // Save the task in the database
  await task.save();

  return task;
}

// Function to delete a task from the list
async function deleteTask(userId, taskId) {
  // Find the task with the provided taskId for the user and delete it
  await Task.findOneAndDelete({ _id: taskId, userId });
}

// Function to mark a task as done
async function markTaskAsDone(userId, taskId) {
  // Find the task with the provided taskId for the user and update its "done" status
  await Task.findOneAndUpdate({ _id: taskId, userId }, { done: true });
}

// Function to update a task
async function updateTask(userId, taskId, updatedTask) {
  // Find the task with the provided taskId for the user and update its properties
  await Task.findOneAndUpdate({ _id: taskId, userId }, updatedTask);
}

// Function to fetch the list of tasks for a user
async function getTasks(userId) {
  const tasks = await Task.find({ userId });
  return tasks;
}

// Function to fetch the list of completed tasks for a user
async function getCompletedTasks(userId) {
  const completedTasks = await Task.find({ userId, done: true });
  return completedTasks;
}

module.exports = {
  addTask,
  deleteTask,
  markTaskAsDone,
  updateTask,
  getTasks,
  getCompletedTasks,
};
