const mongoose = require("mongoose");
const config = require("./config/config");

config.database.connect();

module.exports = mongoose;
