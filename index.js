const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const config = require("./config/config");
const db = require("./db");

// Create an Express app
const app = express();

// Middleware
app.use(cors());
app.use(bodyParser.json());

// Connect to MongoDB
db.then(() => {
  // Start the server after successful database connection
  app.listen(3000, () => {
    console.log("Server is running on port 3000");
  });
}).catch((error) => {
  console.error("Error connecting to MongoDB:", error);
  process.exit(1);
});

// Import and set up routes
const usersRoutes = require("./routes/users");
const tasksRoutes = require("./routes/tasks");

app.use("/api/users", usersRoutes);
app.use("/api/tasks", tasksRoutes);

// Handle 404 - Page Not Found
app.use((req, res, next) => {
  res.status(404).json({ error: "Page not found" });
});

// Handle 500 - Internal Server Error
app.use((err, req, res, next) => {
  console.error("Internal Server Error:", err);
  res.status(500).json({ error: "Internal Server Error" });
});
