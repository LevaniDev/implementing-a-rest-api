const express = require("express");
const router = express.Router();
const UsersService = require("../services/UsersService");
const { authenticateUser } = require("../middleware/auth");

// Route for user registration
router.post("/register", async (req, res) => {
  try {
    const { email, password, firstName, lastName } = req.body;
    const user = await UsersService.registerUser(
      email,
      password,
      firstName,
      lastName
    );
    res.status(201).json(user);
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
});

// Route for user login
router.post("/login", async (req, res) => {
  try {
    const { email, password } = req.body;
    const user = await UsersService.loginUser(email, password);
    res.status(200).json(user);
  } catch (error) {
    res.status(401).json({ error: "Invalid credentials" });
  }
});

module.exports = router;
