const express = require("express");
const router = express.Router();
const TasksService = require("../Services/TasksService");
const { authenticateUser } = require("../Middleware/auth");

// Route for adding a task
router.post("/", authenticateUser, async (req, res) => {
  try {
    const { title, description } = req.body;
    const userId = req.user.id;
    const task = await TasksService.addTask(userId, title, description);
    res.status(201).json(task);
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
});

// Route for deleting a task
router.delete("/:taskId", authenticateUser, async (req, res) => {
  try {
    const { taskId } = req.params;
    const userId = req.user.id;
    await TasksService.deleteTask(userId, taskId);
    res.status(204).end();
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
});

// Route for marking a task as done
router.patch("/:taskId/done", authenticateUser, async (req, res) => {
  try {
    const { taskId } = req.params;
    const userId = req.user.id;
    await TasksService.markTaskAsDone(userId, taskId);
    res.status(204).end();
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
});

// Route for updating a task
router.put("/:taskId", authenticateUser, async (req, res) => {
  try {
    const { taskId } = req.params;
    const userId = req.user.id;
    const { title, description } = req.body;
    const updatedTask = { title, description };
    await TasksService.updateTask(userId, taskId, updatedTask);
    res.status(204).end();
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
});

// Route for fetching the list of tasks
router.get("/", authenticateUser, async (req, res) => {
  try {
    const userId = req.user.id;
    const tasks = await TasksService.getTasks(userId);
    res.status(200).json(tasks);
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
});

// Route for fetching the list of completed tasks
router.get("/completed", authenticateUser, async (req, res) => {
  try {
    const userId = req.user.id;
    const completedTasks = await TasksService.getCompletedTasks(userId);
    res.status(200).json(completedTasks);
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
});

module.exports = router;
